# device_asus_X00T

For building OrangeFox for ASUS ZenFone Max Pro (M1) with [fox_9.0 manifest](https://gitlab.com/OrangeFox/Manifest/tree/fox_9.0)

*Clone to device/asus/X00T*

**To build:**
```
. build/envsetup.sh
lunch omni_X00T-eng
mka recoveryimage
```

Kernel: [OrangeFox/Kernels/x00t](https://gitlab.com/OrangeFox/Kernels/x00t)

## Original sources

Device tree forked from: [KudProject/device_asus_X00T-twrp](https://github.com/KudProject/device_asus_X00T-twrp/tree/android-9.0) [saurabhchardereal/device_asus_X00T-twrp](https://github.com/saurabhchardereal/device_asus_X00T-twrp/tree/android-9.0)

Kernel forked from: [KudProject/kernel_asus_sdm660](https://github.com/KudProject/kernel_asus_sdm660/tree/android-9.0)
